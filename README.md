# Contenido

- [Contenido](#contenido)
- [Introducción](#introducción)
- [Estructuras de datos](#estructuras-de-datos)
- [Algoritmos](#algoritmos)
- [Licencia](#licencia)


# Introducción

La idea del este repositorio es servir como un marco de referencia sobre dos temas importantes cuando sé está iniciando en el mundo de la programación, algoritmos y estructuras de datos.

El repositorio no estará enfocados en un lenguaje de programación en particular, trataremos de tener una gran variedad de lenguajes, hasta donde nuestros conocimientos nos permitan. Siempre estaremos abiertos a que la comunidad colabore con nuevos lenguajes o temas dentro de este repositorio.

Los lenguajes que esta cubiertos en este momento son:

* C/C++
* Python
* Rust
* Java

Otro tema importatente para nosotros es que este repositorio siempre estará 100% en español.

# Estructuras de datos

# Algoritmos

# Licencia

MIT